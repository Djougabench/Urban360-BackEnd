/* eslint-disable no-unused-expressions */
const { contract, accounts } = require('@openzeppelin/test-environment');
const { BN, singletons } = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

const UrbanToken = contract.fromArtifact('UrbanToken');

describe('UrbanToken', function () {
  this.timeout(0);

  const [owner, dev, artist1, investor1] = accounts;
  const NAME = 'URBAN';
  const SYMBOL = 'UBN';
  const artistTest1 = [
    'Lionel',
    'Djouhan',
    '41',
    'deehans4@gmail.com',
    new BN(1),
    '0xF240A9D087bDa31ec2C334e8245c8152911E72a8',
    'true',
  ];
  const investorTest1 = [
    'Bench',
    'Jouga',
    '34',
    'jougabench@mail.com',
    new BN(1),
    '0x15294c91d24d2c4949d8193C705de6D65a3d26e3',
    'true',
  ];

  beforeEach(async function () {
    this.urbantoken = await UrbanToken.new(owner, { from: dev });
  });

  it('Has a name', async function () {
    expect(await this.urbantoken.name()).to.equal(NAME);
  });
  it('Has a symbol', async function () {
    expect(await this.urbantoken.symbol()).to.equal(SYMBOL);
  });

  it('Artist created', async function () {
    await this.urbantoken.createArtist(
      artistTest1[0],
      artistTest1[1],
      artistTest1[2],
      artistTest1[3],
      artistTest1[4],
      artistTest1[5],
      {
        from: artist1,
      },
    );
    const ArtistNew1 = await this.urbantoken.getArtistInfo({ from: artist1 });
    expect(ArtistNew1[0]).to.equal(artistTest1[0]);
    expect(ArtistNew1[1]).to.equal(artistTest1[1]);
    expect(ArtistNew1[2]).to.equal(artistTest1[2]);
    expect(ArtistNew1[3]).to.equal(artistTest1[3]);
    expect(ArtistNew1[4]).to.equal(artistTest1[4]);
    expect(ArtistNew1[5]).to.equal(artistTest1[5]);
  });
});
