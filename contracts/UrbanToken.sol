// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

/**
 * @title Smart contract  production Musicale
 * @author Djouhan Lionel
 * @notice Ce contrat permet mettre à disposition des services aux artistes
 * contre des point de production musicale.
 */
contract UrbanToken is ERC721, Ownable {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds; /// @notice Counter pour pouvoir generer un TOKEN ID et le lier à l'investor
    Counters.Counter private _artistIds; /// @notice counter pour gerer ID artist
    Counters.Counter private _investorIds; /// @notice counter pour generer ID investors

    /**
     * @dev IInitialise le contrat en définissant un «nom» et un «symbole» dans la collection de Token.
     */
    constructor(address payable superAdmin) public ERC721("URBAN", "UBN") {
        //@notice Adress de la personne qui déploie le contrat
        _superAdmin = superAdmin;
    }

    //@notice Adress de la personne qui déploie le contrat
    address payable private _superAdmin;

    /**
     * @notice variable d'etat pour compter Artists
     */
    uint256 public counterArtist;

    /**
     * @notice variable d'etat pourr compter Investisseurs ( ne pas depasser 6 par artiste)
     */
    uint256 public counterInvestor;

    /**
     * @dev enum JobNeeded , les differentes prestations dont l'artist a besoin
     * @notice liste des prestations dont a besoin l'artiste
     */
    enum JobNeeded {lawyer, Photographer, musicProducer, studioOwner, videoDirector, communication}

    /**
     * @dev enum JobInvestor, les  diffeerents corps de metiers imposés que les investissuer peuvent proposer aux artists
     * @notice liste des différents coprs de metiers disponible pour l'artiste via les investisseur
     */
    enum JobInvestor {lawyer, Photographer, musicProducer, studioOwner, videoDirector, communication}

    /**
     * @dev struct theArtist
     */
    struct theArtist {
        string name; // NPrenom de l'artiste
        string lastname; // Prenom de l'artiste
        uint8 age; // age  Artiste
        string mail; //mail Artiste
        JobNeeded jobNeeded; // Prestations dont l'artiste a besoin via enum "jobNeeded"
        bool isArtist; // boolean = false par default  true si Artiste enregistré
    }

    /**
     * @dev struct InfoInvestt
     */
    struct Investor {
        string name; //Prenom de l'investisseur
        string lastname; //Nom de l'investisseur
        uint8 age; //age investisseur
        string mail; //mail investisseur
        JobInvestor jobInvestor; //job de l'investisseur
        address investAddr; //adress ether de l'investisseur
        bool isInvestor; //boolean = false par default  true si investisseur enregistré
    }
    /**
     * @dev struct liste des prestations dont l'artiste a besoin
     */
    struct InvestNededByArtist {
        string Lawyer;
        string Photographe;
        string musicProducer;
        string studioOwner;
        string videoDirector;
        string Communication;
    }

    /**
     * @dev mapping address vers struct Artist
     */
    mapping(address => theArtist) private _artist;

    /**
     * @dev mapping uint vers address
     */
    mapping(uint256 => address) private _Artist;

    /**
     * @dev mapping address vers struct Investor
     */
    mapping(address => Investor) private _investor;

    /**
     * @dev mapping uint vers enum InvestNededByArtist
     */
    mapping(uint256 => InvestNededByArtist) private _InvestNededByArtist;

    /**
     * @dev event profile artist approuvé par superAdmin
     */
    event ApprovedArt(address indexed _address);

    /**
     * @dev event profile investor approuvé par superAdmin
     */
    event ApprovedInv(address indexed _address);

    /**
     * @dev event profile Artist creé
     */
    event ArtistCreated(address indexed _address);

    /**
     * @dev profile investor créé
     */
    event InvestorCreated(address indexed _address);

    /**
     * @dev modifier pour controle exclusif superAdmin
     */
    modifier isSuperAdmin() {
        require(msg.sender == _superAdmin, "you can't use this function");
        _;
    }

    /** @dev modifier pour verifier si artiste est dejà enregistré en tant que artist
     * il faut qu'il ne soit pas déjà inscrit.
     */
    modifier notRegisteredA() {
        require(_artist[msg.sender].isArtist == false, "You are already registered");
        _;
    }

    /**
     * @dev verification si investisseur est déjà inscrit en tant que investisseur
     * il faut qu'il ne soit pas déjà inscrit.
     */
    modifier notRegisteredI() {
        require(_investor[msg.sender].isInvestor == false, "You are already registered");
        _;
    }

    /**
    * @dev modifier verifie que l'investisseur soit enregistré.
    * il doit etre enregistré .
                                             
    modifier onlyInvestor(){
       require(_investor[msg.sender].isInvestor == true ,"youre are not registered as an investor ");
    _;  
       
    }

    */

    /**
     * @dev Permet de register un nouvel Artiste en vérifiant qu'il ne soit pas dejà inscrit
     * @param _name set le Prenom de l'artiste dans la struct theArtist
     * @param _lastname set le nom de famille dans la struct theArtist
     * @param _age set l'age  de l'artiste dans la struct theArtist    *
     * @param _mail set le mail de l'artiste la struct theArtist
     * @param _jobNeeded set le  corps de metier dont a besoin l'artiste  dans la struct theArtist
     */
    function createArtist(
        string memory _name,
        string memory _lastname,
        uint8 _age,
        string memory _mail,
        JobNeeded _jobNeeded
    ) public notRegisteredA() {
        counterArtist++;
        _artistIds.increment();
        _artist[msg.sender] = theArtist({
            name: _name,
            lastname: _lastname,
            age: _age,
            mail: _mail,
            jobNeeded: _jobNeeded,
            isArtist: true
        });

        emit ArtistCreated(msg.sender);
    }

    /**
     *  @dev artistId pour récuperer  l'id de l'artist
     *  @return  un number qui est l'id de l'artiste
     */
    function artistId() public view returns (uint256) {
        return _artistIds.current();
    }

    /**
     *  @dev pour récuperer toutes les infos de l'artiste à l'aide de son address  Ether via la struct Artist
     *  apres enregistrement
     *  de l'artiste dans le formulaire
     *  @return  la struct theArtist complete avec infos de  l'artiste
     */
    function getArtistInfo() public view returns (theArtist memory) {
        return _artist[msg.sender];
    }

    /**
     *  @dev pour récuperer toutes les infos de l'artiste avec son address Ether
     *  @return la struct Investor complete avec infos de  l'investisseur .
     */

    function getArtistInfoVisit(address _addr) public view returns (theArtist memory) {
        return _artist[_addr];
    }

    /**
     * @dev Permet de register un nouvel investisseur en vérifiant qu'il ne soit pas dejà inscrit
     * @param _name set le Prenom de  L'investisseur dans la struct Investor
     * @param _lastname set son nom de famille dans la struct Investor
     * @param  _age set l'age  de l'investisseur dans la struct Investor
     * @param _jobInvestor le  corps de metier dont a besoin l'artiste  dans la struct theArtist
     * @param _mail set le mail de l'investisseur dans la struct Investor
     * @param _investAddr set l'address Ether de l'investisseur  dans la struct Investor
     *
     */
    function createInvestor(
        string memory _name,
        string memory _lastname,
        uint8 _age,
        JobInvestor _jobInvestor,
        string memory _mail,
        address _investAddr
    ) public notRegisteredI() {
        counterInvestor++;

        _investorIds.increment();

        _investor[msg.sender] = Investor({
            name: _name,
            lastname: _lastname,
            age: _age,
            jobInvestor: _jobInvestor,
            mail: _mail,
            investAddr: _investAddr,
            isInvestor: true
        });

        emit InvestorCreated(msg.sender);
    }

    /**
     *  @dev pour récuperer  l'id de linvestisseur
     *  @return un number qui est l'id de l'investisseur
     */
    function getinvestorId() public view returns (uint256) {
        return _investorIds.current();
    }

    /**
     *  @dev pour récuperer toutes les infos de l'investisseur avec son address Ether
     *  @return la struct Investor complete avec infos de  l'investisseur lors du register du owner
     */
    function getInvestorInfo() public view returns (Investor memory) {
        return _investor[msg.sender];
    }

    /**
     *  @dev pour récuperer toutes les infos de l'investisseur avec son address Ether
     *  @return la struct Investor complete avec infos de  l'investisseur .
     */

    function getInvestorInfoVisit(address _addr) public view returns (Investor memory) {
        return _investor[_addr];
    }

    /**
     *  @dev fonction pour approuver profil artiste
     *  @return un boolean et emit dans l'event ApprovedArt
     *  qui nous informe que l'artiste est approuvé.
     */

    function approveArtist(address _addr) public isSuperAdmin() returns (bool) {
        _artist[_addr].isArtist = true;
        emit ApprovedArt(_addr);
    }

    /**
     *  @dev fonction pour checker si le profil de l'artiste est approuvé
     *  @return la struct theArtist modifié en true si approuvé
     *  ou false si toujours pas approuvé
     */
    function checkIfApprovedArtist(address _addr) public view isSuperAdmin() returns (theArtist memory) {
        return _artist[_addr];
    }

    /**
     *  @dev fonction pour approuver profil investisseur
     *  @return un boolean et emit dans l'event ApprovedInv
     *  qui nous informe que l'investisseur est approuvé.
     */
    function approveInvest(address _addr) public isSuperAdmin returns (bool) {
        _investor[_addr].isInvestor = true;

        emit ApprovedInv(_addr);
    }

    /**
     *  @dev fonction pour checker si le profil de l'investisseur est approuvé
     *  @return la struct Investor modifié en true si approuvé
     *  ou false si toujours pas approuvé
     */
    function checkIfApprovedInvest(address _addr) public view isSuperAdmin() returns (Investor memory) {
        return _investor[_addr];
    }

    /**
     *  @dev recuperer les differents corps de metier dont l'artiste a besoin via son ID,
     *  @return la struct InvestNededByArtist
     */
    function getInvestNededByArtistId(uint256 _id) public view returns (InvestNededByArtist memory) {
        return _InvestNededByArtist[_id];
    }

    /**
     *  @dev cré un token ID associé à l'adresse  de l'artist et de l'investisseur
     *  @return un number
     */
    function ubnTokenArtist(address investor, address _addressArtist)
        public
        onlyOwner /*onlyInvestor */
        returns (uint256)
    {
        //(à faire )require(_investor[investor].jobInvestor == _artist[_addressArtist].jobNeeded,"Your job have to be the same the artist need. ");

        require(_investor[investor].isInvestor == true, "youre are not registered as an investor ");
        _tokenIds.increment();
        uint256 newTokenId = _tokenIds.current();
        _mint(investor, newTokenId);
        _Artist[newTokenId] = _addressArtist; // ID TOKEN lié à l'adresse artist
        return newTokenId;
    }

    /**
     *  @dev retrouve l'address Ether de l'artiste grace à son token ID
     *  @return  _addressArtist qui l'addresse Ether de l'artiste
     */
    function getArtistByTokenId(uint256 tokenId) public view returns (address _addressArtist) {
        require(_exists(tokenId), "URBAN: UBN query for nonexistent token");
        return _Artist[tokenId];
    }
}
